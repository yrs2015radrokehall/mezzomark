var directionsDisplay;
var infoWindow
var directionsService = new google.maps.DirectionsService();
var map;
var directionsDisplay;
var directionsService;
var stepDisplay;
var markerArray = [];
var service
var selectedTypes = []
var radiusLabelValue

function initialize() {

  $( "#getRoute" ).click(function() {
    check()
    calcRoute()
    jQuery('html, body').animate({scrollTop: 500}, 400);
  });
  checkRadiusSlider()

  autocompleteStart = new google.maps.places.Autocomplete(
     /** @type {HTMLInputElement} */(document.getElementById('start')),
     {
       types: ['(cities)']
       //componentRestrictions: countryRestrict
     });

     autocompleteEnd = new google.maps.places.Autocomplete(
        /** @type {HTMLInputElement} */(document.getElementById('end')),
        {
          types: ['(cities)']
          //componentRestrictions: countryRestrict
        });


  directionsDisplay = new google.maps.DirectionsRenderer();
  var mapOptions = {
    zoom: 6,
    center: new google.maps.LatLng(53.775159, -1.952032)
  };

  map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);
  directionsDisplay.setMap(map);
  directionsDisplay.setPanel(document.getElementById('directions-panel'));
  var control = document.getElementById('control');
  //control.style.display = 'block';
  map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(BackToTop);
  infowindow = new google.maps.InfoWindow();
}

function checkRadiusSlider() {
  radiusLabelValue = document.getElementById('slider').value
  document.getElementById("radiusLabel").innerHTML = radiusLabelValue
}


function calcRoute() {

  for (var i = 0; i < markerArray.length; i++) {
    markerArray[i].setMap(null);
  };

  var start = document.getElementById('start').value;
  var end = document.getElementById('end').value;
  var service = new google.maps.DistanceMatrixService();
  service.getDistanceMatrix(
    {
      origins:[start],
      destinations: [end],
      travelMode: google.maps.TravelMode.DRIVING,
      unitSystem: google.maps.UnitSystem.METRIC,
    }, callback);

  var request = {
    origin: start,
    destination: end,
    travelMode: google.maps.TravelMode.DRIVING
  };

    directionsService.route(request, function(response, status) {
      if (status == google.maps.DirectionsStatus.OK) {
        var warnings = document.getElementById('warnings_panel');
        showSteps(response);
        directionsDisplay.setDirections(response);
      }
    });

  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    }
  });

  for (var i = 0; i < markerArray.length; i++) {
    markerArray[i].setMap(null);
  }
  markerArray = []

}


function check() {

    // clear current list
    selectedTypes.length = 0;

  // Support types are here: https://developers.google.com/places/supported_types
    if (document.getElementById('switch-1').checked == true) {
      selectedTypes.push('restaurant');
      selectedTypes.push('cafe');
      selectedTypes.push('bar');
      selectedTypes.push('meal_takeaway');
    }
    if (document.getElementById('switch-2').checked == true) {
      selectedTypes.push('art_gallery')
      selectedTypes.push('library')
      selectedTypes.push('museum')
      selectedTypes.push('amusement_park')
      selectedTypes.push('night_club')
    }
    if (document.getElementById('switch-3').checked == true) {
      selectedTypes.push('parking')
      selectedTypes.push('park')
      selectedTypes.push('train_station')
      selectedTypes.push('gas_station')
      selectedTypes.push('airport')
      selectedTypes.push('bus_station')
      selectedTypes.push('lodging')
    }
    if (document.getElementById('switch-4').checked == true) {
      selectedTypes.push('clothing_store')
      selectedTypes.push('bicycle_store')
      selectedTypes.push('casino')
      selectedTypes.push('department_store')
      selectedTypes.push('florist')
      selectedTypes.push('grocery_or_supermarket')
    }

    console.log("Selected Types: ", selectedTypes)
}

function showSteps(directionResult) {
  var myRoute = directionResult.routes[0].legs[0];
  var markerLocations = []

//Calculate the total time and distance for this route
  var total_time_secs = 0;
  var total_dist_m = 0;
  for (var i = 0; i < myRoute.steps.length; i++) {
    var step = myRoute.steps[i];
    //console.log(i,step)
    total_time_secs += step.duration.value;
    total_dist_m += step.distance.value;
  }
  console.log('Total time in secs: ',total_time_secs)
  console.log('Total dist in metres: ',total_dist_m)

//Now loop again by stop when you go past helf way (in time)
  var current_time_secs = 0
  var half_way_step = null
  var midway_point = null;
  for (var i = 0; i < myRoute.steps.length; i++) {
    var step = myRoute.steps[i];
    current_time_secs += step.duration.value;
    if( current_time_secs > total_time_secs/2.0){
      half_way_step = step;

      // Calculate the ratio along the step based on time. ie the mid way point of our journey
      // might lie at a third the way (in time) along this step
      var start_step_time = current_time_secs - step.duration.value;  // get the time at the start of the step
      var ratio_along_step = (total_time_secs/2 - start_step_time ) / step.duration.value;
      console.log('The Ratio along middle step is: ',ratio_along_step);

      // Now we need to step along the polyline points until the distance (we dont have time per point)
      // matches the ratio based on time.
      var target_dist = step.distance.value * ratio_along_step;
      console.log('target dist along step is: ', target_dist);
      var dist_along_path = 0.0;
      for( var i=1; i<step.path.length; i++){  // start at 1 not zero
        var seg_length = google.maps.geometry.spherical.computeDistanceBetween(step.path[i-1],step.path[i])
        if ( (dist_along_path + seg_length) > target_dist){  // If this goes past target_dist then we have our segment
          // just use the end point for now (refine later if necessary)
          midway_point = step.path[i];
          console.log('Calculated midway point as: ',midway_point.toString());
          break;
        }
        dist_along_path += seg_length
      }
      if ( midway_point == null ){
        console.log('*** Failed to find Midway Point on half way step!')
      }
      // Weve found what we are looking for so lets get out of here
      break;
    }
  }
  // This leaves us with the step at half way so lets add a marker here to check
    if( half_way_step != null){
      console.log('Half Way Step: ', half_way_step);
  //    var markerLocation = new google.maps.LatLng(half_way_step.end_location.G, half_way_step.end_location.K)
  //    console.log(markerLocation)
      if( midway_point != null){
        var marker = new google.maps.Marker({
          map: map,
          icon: 'yellow_MarkerM.png',
          position: midway_point
        });

        google.maps.event.addListener(marker, 'click', function() {
          var midpoint_secs = total_time_secs / 2.0;
          var hours = parseInt( midpoint_secs / 3600 ) % 24;
          var minutes = parseInt( midpoint_secs / 60 ) % 60;
          var seconds = midpoint_secs % 60;
          s = '<li>This is the mid-point of your journey.</li> <li>It will take both of you <b>'+hours+' hours</b> and <b>'+minutes+' mins</b> to meet each other.</li> <li><b>Drive safely!</b></li>';
          infowindow.setContent(s);
          infowindow.open(map, this);
        })

        getPlace(midway_point)

        markerArray.push(marker);
      }else{
        console.log('*** Found half way step but could not interpoloate midway point on step')
      }
    }else{
      console.log('*** Could not find half way step')
    }

  }



function getPlace(markerLocation) {
  var data = {
      location: markerLocation,
      radius: radiusLabelValue * 1000,
      types: selectedTypes
  };
  console.log('getPlace: ', data);

  var service = new google.maps.places.PlacesService(map);
  service.nearbySearch(data, placeCallback);

}

function placeCallback(results, status) {
 console.log('in placeCallback: ' , status)
 var bounds = new google.maps.LatLngBounds();
 if (status == google.maps.places.PlacesServiceStatus.OK) {
   for (var i = 0; i < results.length; i++) {
     createMarker(results[i]);
     bounds.extend(results[i].geometry.location);
   }
   map.fitBounds(bounds);
  }
}

function createMarker(place) {
  var placeLoc = place.geometry.location;
  //console.log('createMarker:', placeLoc)
  var image = {
    url: place.icon,
    size: new google.maps.Size(71, 71),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(17, 34),
    scaledSize: new google.maps.Size(25, 25)
  };

  var marker = new google.maps.Marker({
    map: map,
    icon: image,
    position: placeLoc
  });

  if (place.vicinity != null) {
    address = place.vicinity
  } else {
    address = 'No address availible'
  }


  var infoWindowContent = '<img src="'+place.icon+'"" style="width:24px;height:24px;"><li><b>'+place.name+'</b></li><li>'+place.vicinity+'</li>';
  infoWindowContent +=    '<li><a target="_blank" href="http://www.google.co.uk/search?q='+place.name+','+place.vicinity+' ">Details</a></li>';

  google.maps.event.addListener(marker, 'click', function() {
    infowindow.setContent(infoWindowContent);
    infowindow.open(map, this);
  })

  markerArray.push(marker)
}

function callback(response, status) {
  var origins = response.originAddresses;
  var destinations = response.destinationAddresses;
  var distance = 0

  for (var i = 0; i < origins.length; i++) {
      var results = response.rows[i].elements;
      for (var j = 0; j < results.length; j++) {
          console.log(results[j].distance.text)
          distance = parseFloat(results[j].distance.text.replace(/,/g , ''))
          console.log(distance)
          console.log('Halfway:')
          var halfway = distance / 2
          console.log(halfway)
          console.log(google.maps.decodePath)
      }
    }
}

function attachInstructionText(marker, text) {
  google.maps.event.addListener(marker, 'click', function() {
    // Open an info window when the marker is clicked on,
    // containing the text of the step.
    stepDisplay.setContent(text);
    stepDisplay.open(map, marker);
  });
}

google.maps.event.addDomListener(window, 'load', initialize);

jQuery(document).ready(function() {
   var offset = 250;
   var duration = 300;

   jQuery(window).scroll(function() {
     if (jQuery(this).scrollTop() > offset) {
       jQuery('.back-to-top').fadeIn(duration);
     } else {
       jQuery('.back-to-top').fadeOut(duration);
     }
   });
   jQuery('.back-to-top').click(function(event) {
     event.preventDefault();
     jQuery('html, body').animate({scrollTop: 0}, 400);
     return false;
   })
});
